#include<iostream>
#include<cstdio>
using namespace std;

long int ck=0;

int length(long int n)
{
    while(n>0){
        ++ck;
        if(n==1)
            return ck;
        else if(n%2==0)
            n=n/2;
        else
            n=3*n+1;
    }
    return ck;
}

int main()
{
    long int a, b, i, mx=0,temp;
    while(scanf("%ld%ld", &a, &b)!=EOF){
          if(a>b){
            for(i=b;i<=a;++i){
                temp=length(i);
                if(temp>mx)
                    mx=temp;
                ck=0;
            }
            printf("%ld %ld %ld\n", a, b, mx);
            mx=0;
        }
        else{
            for(i=a;i<=b;++i){
                temp=length(i);
                if(temp>mx)
                    mx=temp;
                ck=0;
            }
            printf("%ld %ld %ld\n", a, b, mx);
            mx=0;
        }
    }
    return 0;

}
